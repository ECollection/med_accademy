﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ma.EntityLibrary.Data
{
    public class StudentViewList
    {
        public long GroupID { get; set; }
        public long StudentId { get; set; }
        public string StudentIDData { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
    }
}
