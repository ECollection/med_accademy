﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ma.ClassLibrary.Utility;

namespace Ma.EntityLibrary.Data
{
    public class PreparationUser : BaseReference
    {
        private tb_Preparation preparations;
        public PreparationUser() { }
        public PreparationUser(tb_Preparation crs) { preparations = crs; }
        public PreparationUser(long examid) { preparations = _Entities.tb_Preparation.FirstOrDefault(x => x.PreparationId == examid); }
        public long PreparationId { get { return preparations.PreparationId; } }
        public long ExamId { get { return preparations.ExamId; } }
        public string StartDate { get { return preparations.StartDate; } }
        public string EndDate { get { return preparations.EndDate; } }
        public bool IsActive { get { return preparations.IsActive; } }
        public System.DateTime Timestamp { get { return preparations.Timestamp; } }
        public Nullable<System.TimeSpan> Duration { get { return preparations.Duration; } }
        public string PreparationName { get { return preparations.PreparationName; } }
    }
}
