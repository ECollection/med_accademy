//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ma.EntityLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_Level
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tb_Level()
        {
            this.tb_Question = new HashSet<tb_Question>();
        }
    
        public long LevelID { get; set; }
        public long SubTopicID { get; set; }
        public string LevelName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Timestamp { get; set; }
    
        public virtual tb_Subtopic tb_Subtopic { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_Question> tb_Question { get; set; }
    }
}
