﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medacademy.Models
{
    public class DropGroup
    {
        public string GroupName { get; set; }
        public long GroupID { get; set; }
    }
}