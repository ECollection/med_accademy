﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medacademy.Models
{
    public class ViewPdf
    {
        public string pdfdata { get; set; }
        public string videodata { get; set; }
        public string videotype { get; set; }
    }
}