﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medacademy.Models;
using Ma.ClassLibrary;
using Ma.EntityLibrary;
using System.IO;
using Ma.EntityLibrary.Data;
using System.Globalization;

namespace Medacademy.Controllers
{
    public class ReportController : Controller
    {
        public DateTime CurrentTime = TimeZoneInfo.ConvertTimeFromUtc(System.DateTime.Now.ToUniversalTime(), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
        public MAEntities _Entities = new MAEntities();
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserExamReult()
        {         
            return View();
        }

        public ActionResult ExamWiseReport(string id)
        {           
            ExamModel model = new ExamModel();
            model.ExamID = Convert.ToInt64(id);
            return View(model);
        }
        public ActionResult ExamReview(string id)
        {
            var userData = (Medacademy.Models.UserModel)Session["UserLoginFirstTime"];
            string[] splitData = id.Split('~');
            SubmitExamModel model = new SubmitExamModel();
            model.userId = userData.UserId;          
            model.ExamId = Convert.ToInt64(splitData[0]);
            model.QuestionId = Convert.ToInt64(splitData[1]);
            ViewBag.Navigate = 1.ToString();
            //return View("StartPractice", model);
            return View(model);
        }
    }
}