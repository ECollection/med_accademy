﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.specialChars = config.specialChars.concat(['√', '∛', '×', '÷', '∴', '∵', '∇', 'Δ', '≠', 'π', '∠', '∑', '±', '∪', '∩', '∈', '∉', '⊂', '∃', '∀', '∅', '⊥', '≤', '≥', '₹', '𝓧', '𝔁', 'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', '≅', '±', '∓']);
//    config.specialChars = config.specialChars.concat(['×']);
//    config.specialChars = config.specialChars.concat(['÷']);
//    config.specialChars = config.specialChars.concat(['∴']);
//    config.specialChars = config.specialChars.concat(['∛']);
};
