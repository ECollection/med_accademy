﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ma.ClassLibrary.Utility
{
    public class DropGroup
    {
        public string GroupName { get; set; }
        public long GroupID { get; set; }
    }
}
